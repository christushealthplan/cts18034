/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function() {
    if (typeof window.CustomEvent === 'function') { return false; }

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {

    var tl;
    var win = window;

    function doClickTag() { window.open(window.clickTag); }

    function initTimeline() {
        document.querySelector('#ad .banner').style.display = 'block';
        document.getElementById('ad').addEventListener('click', doClickTag);
        createTimeline();
    }

    function createTimeline() {
        tl = new TimelineMax({delay: 0.25, onStart: updateStart, onComplete: updateComplete, onUpdate: updateStats});
        // ---------------------------------------------------------------------------

        tl.add('frame1')
        .to("#ribbonmask", 8, {attr:{x:0}, ease: Power2.easeInOut}, 'frame1')
        .from('.hl1a', 2, {x:"-15%", ease: Power3.easeOut }, 'frame1+=0.5')
        .from('.hl1a', 2, {opacity: 0, ease: Power3.easeOut }, 'frame1+=0.5')
        .to('.hl1a', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=2.5')
        .from('.hl1b', 2, {x:"-15%", ease: Power3.easeOut }, 'frame1+=1')
        .from('.hl1b', 2, {opacity: 0, ease: Power3.easeOut }, 'frame1+=1')
        .to('.hl1b', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=2.5')
        .from('.hl2a', 2, {x:"-15%", ease: Power3.easeOut }, 'frame1+=3.5')
        .from('.hl2a', 2, {opacity: 0, ease: Power3.easeOut }, 'frame1+=3.5')
        .to('.hl2a', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=5.5')
        .from('.hl2b', 2, {x:"-15%", ease: Power3.easeOut }, 'frame1+=4')
        .from('.hl2b', 2, {opacity: 0, ease: Power3.easeOut }, 'frame1+=4')
        .to('.hl2b', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=5.5')
        .from('.hl3a', 2, {x:"-15%", ease: Power3.easeOut }, 'frame1+=6.5')
        .from('.hl3a', 2, {opacity: 0, ease: Power3.easeOut }, 'frame1+=6.5')
        .to('.hl3a', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=9')
        .from('.hl3b', 2, {x:"-15%", ease: Power3.easeOut }, 'frame1+=7')
        .from('.hl3b', 2, {opacity: 0, ease: Power3.easeOut }, 'frame1+=7')
        .to('.ribbon', 0.5, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=9.5')
        .to('.hl3b', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=9')
        .to('.logo', 1, { opacity: 0, ease: Power3.easeInOut }, 'frame1+=9.5')
        .from('.background', 1, { opacity: 0, ease: Power3.easeInOut }, 'frame1+=9.5')
        .from('.logo1', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=9.5')
        .to('.logo1', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=12.75')
        .from('.hl4', 2, {x:"-15%", ease: Power3.easeOut }, 'frame1+=10.5')
        .from('.hl4', 2, {opacity: 0, ease: Power3.easeOut }, 'frame1+=10.5')
        .to('.hl4', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=12.75')
        .from('.logo2', 1.25, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=13.5')
        .from('.cta', 1, { opacity: 0, ease: Power3.easeInOut }, 'frame1+=14')
    


        // ---------------------------------------------------------------------------

        // DEBUG:
        // tl.play('frame3'); // start playing at label:frame3
        // tl.pause('frame3'); // pause the timeline at label:frame3
    }

    function updateStart() {
        var start = new CustomEvent('start', {
            'detail': { 'hasStarted': true }
        });
        win.dispatchEvent(start);
    }

    function updateComplete() {
        var complete = new CustomEvent('complete', {
            'detail': { 'hasStopped': true }
        });
        win.dispatchEvent(complete);
    }

    function updateStats() {
        var statistics = new CustomEvent('stats', {
            'detail': { 'totalTime': tl.totalTime(), 'totalProgress': tl.totalProgress(), 'totalDuration': tl.totalDuration()
            }
        });
        win.dispatchEvent(statistics);
    }

    function getTimeline() {
        return tl;
    }

    return {
        init: initTimeline,
        get: getTimeline
    };

})();

// Banner Init
// ====================================================================================================
timeline.init();
